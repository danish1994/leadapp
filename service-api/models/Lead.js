const mongoose = require('mongoose');
const {setLeadScore} = require("../service/Lead");
const {PREFERRED_CONTACT_TYPES} = require("../constants");
const {Schema} = mongoose;

const LeadSchema = new Schema({
    name: {type: String},
    phone: {type: String, index: true},
    city: {type: String, index: true},
    state: {type: String, index: true},
    zip: {type: String, index: true},
    preferredContact: {type: String, index: true, enum: Object.values(PREFERRED_CONTACT_TYPES)},

    // Meta Value
    score: {type: Number, index: true}, // Score is assigned on the basis of how likely the lead will by (The Algorithm)
    createdAt: {type: Date, default: Date.now} // Created At
});

LeadSchema.pre('save', function (next) {
    // Calculate Score (The Algorithm)
    setLeadScore(this);
    next();
});

module.exports = mongoose.model('Lead', LeadSchema);
