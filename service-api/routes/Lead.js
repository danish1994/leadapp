const express = require('express')
const {addLead, getLeads, updateScores} = require("../controller/LeadController");
const router = express.Router()

router.post('/', addLead);
router.get('/', getLeads);

router.get('/updateScores', updateScores);

module.exports = {router, basePath: 'lead'};
