// Environment Setup
require('dotenv').config()

const mongoose = require('mongoose');
const express = require('express');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser')

const PORT = process.env.PORT || 3000;
const API_VERSION = process.env.API_VERSION || 'v1';

const initDb = async () => {
    await mongoose.connect(process.env.MONGO_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    });

    // await initDbModels();
}

const initDbModels = async () => {
    const modelsDir = path.join(__dirname, './models');
    const models = fs.readdirSync(modelsDir);

    for (const model of models) {
        const modelPath = path.join(modelsDir, model);
        require(modelPath);
    }
}

const initServer = async () => {
    return new Promise(async (res) => {
        const app = express();
        applyMiddleware(app);
        await initRoutes(app);
        app.listen(PORT, res);
    })
}

const applyMiddleware = (app) => {
    app.use(bodyParser.json());
    enableCORS(app);
}

const enableCORS = (app) => {
    app.options('*', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Credentials', true);
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
        res.end();
    });
    app.use('*', (req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        next();
    });
}

const initRoutes = async (app) => {
    const routesDir = path.join(__dirname, './routes');
    const routes = fs.readdirSync(routesDir);
    const baseApiPath = `/api/${API_VERSION}`

    for (const route of routes) {
        const routePath = path.join(routesDir, route);
        const {router, basePath} = require(routePath);
        app.use(`${baseApiPath}/${basePath}`, router);
    }
}

const init = async () => {
    await initDb();
    await initServer();

    return PORT;
}


module.exports = {
    init
};
