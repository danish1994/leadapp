const Joi = require('joi');
const ResponseBuilder = require('../utils/ResponseBuilder');
const Lead = require('../models/Lead');
const {setLeadScore} = require("../service/Lead");
const {PREFERRED_CONTACT_TYPES, STATES} = require('../constants');

const addLead = async (req, res) => {
    const response = new ResponseBuilder();

    try {
        const {body} = req;
        const leadSchema = Joi.object({
            name: Joi.string().required(),
            phone: Joi.string().required(),
            city: Joi.string().required(),
            state: Joi.string().valid(...Object.keys(STATES)).required(),
            zip: Joi.string().required(),
            preferredContact: Joi.string().valid(...Object.values(PREFERRED_CONTACT_TYPES)).required(),
        });

        const value = await leadSchema.validateAsync(body);

        const newLead = new Lead(value);
        await newLead.save()

        response.setData('Lead Saved Successfully');
    } catch (e) {
        response.setError(e.message);
    }

    res.send(response.getResponse());
};

const getLeads = async (req, res) => {
    const response = new ResponseBuilder();
    try {
        const data = await Lead
            .find({})
            .sort({score: -1})
            .lean();

        response.setData(data);
    } catch (e) {
        response.setError(e.message);
    }

    res.send(response.getResponse());
}


const updateScores = async (req, res) => {
    const response = new ResponseBuilder();
    try {
        const data = await Lead
            .find({})
            .sort({score: -1})

        for (const lead of data) {
            lead.score = setLeadScore(lead);
            await lead.save();
        }

        response.setData('Scores Updated');
    } catch (e) {
        response.setError(e.message);
    }

    res.send(response.getResponse());
}


module.exports = {
    addLead,
    getLeads,
    updateScores
}
