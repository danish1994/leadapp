const {PREFERRED_CONTACT_TYPES, CONTIGUOUS_STATE_CODES} = require('../constants');

const setLeadScore = (lead) => {
    lead.score = 0;

    // People whose zip code starts with 7 are highly likely to buy.
    if (lead.zip.charAt(0) === '7') {
        lead.score += 1;
    }

    // People who have a "Z" in their names are highly likely to buy.
    if (lead.name.charAt(0) === 'Z' || lead.name.charAt(0) === 'z') {
        lead.score += 1;
    }

    // The sales team tends to make more sales over the phone than by any other method.
    if (lead.preferredContact === PREFERRED_CONTACT_TYPES.PHONE) {
        lead.score += 1;
    }

    // We've only ever made one sale by carrier pigeon.
    if (lead.preferredContact === PREFERRED_CONTACT_TYPES.PIGEON) {
        lead.score = -1;
    }

    // People outside the contiguous 48 states are ineligible to buy.
    if (!CONTIGUOUS_STATE_CODES.includes(lead.state)) {
        lead.score = -2;
    }
}

module.exports = {
    setLeadScore
}
