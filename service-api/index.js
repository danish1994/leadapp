const {init} = require('./bootstrap');

init()
    .then((port) => {
        console.log('Server Running On:', port);
    })
    .catch((e) => {
        console.log('Error Starting Server');
        console.log(e);
    })
