class ResponseBuilder {
    constructor() {
        this.data = {};
        this.success = true;
        this.error = null;
    }


    setData(data) {
        this.data = data;
    }

    setError(error) {
        this.error = error;
        this.success = false;
    }

    getResponse() {
        return {
            data: this.data,
            success: this.success,
            error: this.error
        }
    }
}

module.exports = ResponseBuilder;
