# Lead App

This contains the following parts

* Backend - `service-api`
* Frontend - `service-web`

## Running the Application

* Install the following.
    * `node`
    * `mongodb`
    * `yarn` (for running frontend)
* Running Frontend
    * Navigate to `service-web`
    * Install Dependencies `yarn install`
    * Run the Application `yarn start`
* Running Backend
    * Navigate to `service-api`
    * Install Dependencies `npm i`
    * Add DB path in `.env` as `MONGO_URL`
    * Run the Application `npm start`
