import React from 'react';
import {Field} from "redux-form";
import TextField from "@material-ui/core/TextField";

export default function TextFormField(props) {
    const {name, validate, warn, ...restProps} = props;
    return <Field
        name={name}
        validate={validate}
        warn={warn}
        component={({input, meta}) => {
            const {visited, error} = meta;
            const hasError = Boolean(visited && error);
            return <React.Fragment>
                <TextField
                    error={hasError}
                    {...input}
                    {...restProps}
                    helperText={hasError && error}
                />
            </React.Fragment>
        }}
    />
}
