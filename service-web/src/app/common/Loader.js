import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    loaderContainer: {
        display: 'flex',
        height: '100vh',
        width: '100vw',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        background: 'rgba(0,0,0,0.2)',
        zIndex: 5,
        top: 0,
        left: 0
    },
    loader: {
        flex: 1,
        height: '15vh'
    }
}))

export const Loader = (props) => {
    const classes = useStyles();

    const {loading} = props;

    if (loading) {
        return <div className={classes.loaderContainer}>
            <img className={classes.loader} src={require('../assets/loader.svg')} alt={'Loader'}/>
        </div>
    }

    return <React.Fragment/>
};
