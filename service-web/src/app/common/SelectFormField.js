import React from 'react';
import {Field} from "redux-form";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import makeStyles from "@material-ui/core/styles/makeStyles";
import FormHelperText from "@material-ui/core/FormHelperText";

const useStyles = makeStyles((theme) => ({
    selectControl: {
        minWidth: 120,
        width: '100%',
    },
}));


export default function SelectFormField(props) {
    const classes = useStyles();
    const {name, validate, warn, options, label, variant, ...restProps} = props;
    return <Field
        name={name}
        validate={validate}
        warn={warn}
        component={({input, meta}) => {
            const {visited, error} = meta;
            const hasError = Boolean(visited && error);
            const inputLabelId = `label-${name}`;
            return <FormControl variant={variant} error={hasError} className={classes.selectControl} key={name}>
                <InputLabel id={inputLabelId}>{label}</InputLabel>
                <Select
                    {...input}
                    {...restProps}
                    labelId={inputLabelId}
                    label={label}
                >
                    {
                        options.map(({value, label}) => {
                            return <MenuItem value={value} key={value}>{label}</MenuItem>
                        })
                    }
                </Select>
                {hasError && <FormHelperText>{error}</FormHelperText>}
            </FormControl>
        }}
    />
}
