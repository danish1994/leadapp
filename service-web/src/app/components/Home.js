import React from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    heroContent: {
        padding: theme.spacing(10, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    gridContainer: {
        padding: theme.spacing(2)
    }
}));

export default function Home() {
    const classes = useStyles();
    let history = useHistory();

    return (
        <React.Fragment>
            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <Grid container spacing={5} alignItems="flex-end" className={classes.gridContainer}>
                    <Grid item xs={12} md={6}>
                        <Card>
                            <CardHeader
                                title={'Create Lead'}
                                titleTypographyProps={{align: 'center'}}
                                subheaderTypographyProps={{align: 'center'}}
                                className={classes.cardHeader}
                            />
                            <CardContent>
                                <ul>
                                    <Typography component="li" variant="subtitle1" align="center">
                                        Create New Lead
                                    </Typography>
                                </ul>
                            </CardContent>
                            <CardActions>
                                <Button
                                    fullWidth
                                    color="primary"
                                    variant={'contained'}
                                    onClick={() => {
                                        history.push('/lead/add')
                                    }}
                                >
                                    Create Lead
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Card>
                            <CardHeader
                                title={'View Leads'}
                                titleTypographyProps={{align: 'center'}}
                                subheaderTypographyProps={{align: 'center'}}
                                className={classes.cardHeader}
                            />
                            <CardContent>
                                <ul>
                                    <Typography component="li" variant="subtitle1" align="center">
                                        View Leads
                                    </Typography>
                                </ul>
                            </CardContent>
                            <CardActions>
                                <Button
                                    fullWidth
                                    color="primary"
                                    variant={'contained'}
                                    onClick={() => {
                                        history.push('/lead/list')
                                    }}
                                >
                                    View Leads
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </React.Fragment>
    );
}
