import React, {useCallback, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TablePagination from "@material-ui/core/TablePagination";
import TableCell from "@material-ui/core/TableCell";
import Paper from "@material-ui/core/Paper";
import {Loader} from "../common/Loader";
import {getLeads} from "../services/LeadService";
import {PREFERRED_CONTACT_TYPES_2_LABEL_MAPPING, STATES} from "../constants";

const useStyles = makeStyles((theme) => ({
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    paper: {
        padding: theme.spacing(1),
    }
}));

const columns = [
    {id: 'name', label: 'Name'},
    {id: 'phone', label: 'Phone'},
    {id: 'city', label: 'City'},
    {
        id: 'state', label: 'State', format: (value) => STATES[value] || value
    },
    {id: 'zip', label: 'Zip Code'},
    {
        id: 'preferredContact',
        label: 'Preferred Contact',
        format: (value) => PREFERRED_CONTACT_TYPES_2_LABEL_MAPPING[value] || value
    },

];

export default function LeadList() {
    const classes = useStyles();

    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);


    const handleChangePage = useCallback((event, newPage) => {
        setPage(newPage);
    }, [setPage]);

    const handleChangeRowsPerPage = useCallback((event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }, []);

    useEffect(() => {
        getLeads()
            .then((data = []) => {
                setData(data);
                setLoading(false);
            });
    }, []);


    return (
        <React.Fragment>
            <Loader loading={loading}/>
            <Container maxWidth="md" component="main" className={classes.heroContent}>
                <Paper className={classes.paper}>
                    <TableContainer>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    {columns.map((column) => (
                                        <TableCell
                                            key={column.id}
                                        >
                                            {column.label}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                                    return (
                                        <TableRow hover role="checkbox" tabIndex={-1} key={row._id}>
                                            {columns.map((column) => {
                                                const value = row[column.id];
                                                return (
                                                    <TableCell key={column.id} align={column.align}>
                                                        {column.format ? column.format(value) : value}
                                                    </TableCell>
                                                );
                                            })}
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={data.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </Paper>
            </Container>
        </React.Fragment>
    );
}
