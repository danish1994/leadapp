import React, {useCallback, useState} from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Paper from "@material-ui/core/Paper";
import {reduxForm} from "redux-form";

import TextFormField from '../common/TextFormField';
import {phoneNumber, required} from "../utils";
import SelectFormField from "../common/SelectFormField";
import {PREFERRED_CONTACT_TYPES_2_LABEL_MAPPING, STATE_OPTIONS} from "../constants";
import {saveLead} from "../services/LeadService";
import {Loader} from "../common/Loader";
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(5)
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function AddLead(props) {
    const classes = useStyles();
    const {handleSubmit, invalid, submitting} = props;

    return (
        <Container component="main" maxWidth="sm">
            <CssBaseline/>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Add Lead Form
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit} autoComplete="off">
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextFormField
                                variant="outlined"
                                required
                                fullWidth
                                id="name"
                                label="Name"
                                name="name"
                                validate={[required]}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextFormField
                                variant="outlined"
                                required
                                fullWidth
                                id="phone"
                                label="Phone"
                                name="phone"
                                validate={[required, phoneNumber]}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextFormField
                                variant="outlined"
                                required
                                fullWidth
                                name="city"
                                label="City"
                                id="city"
                                validate={[required]}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <SelectFormField
                                variant="outlined"
                                required
                                fullWidth
                                name="state"
                                label="State"
                                id="state"
                                validate={[required]}
                                options={STATE_OPTIONS}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextFormField
                                variant="outlined"
                                required
                                fullWidth
                                name="zip"
                                label="Zip"
                                id="zip"
                                validate={[required]}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <SelectFormField
                                variant="outlined"
                                required
                                name="preferredContact"
                                label="Preferred Contact"
                                id="preferredContact"
                                validate={[required]}
                                options={Object.entries(PREFERRED_CONTACT_TYPES_2_LABEL_MAPPING).map(([value, label]) => ({
                                    label,
                                    value
                                }))}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={invalid || submitting}
                    >
                        Add Lead
                    </Button>
                </form>
            </Paper>
        </Container>
    );
}

const AddLeadForm = reduxForm({
    form: 'addLead'
})(AddLead)

export default function AddLeadComponent(props) {
    const [loading, setLoading] = useState(false);
    const history = useHistory();

    const onSubmit = useCallback((formValue) => {
        setLoading(true)
        saveLead(formValue)
            .then(() => {
                history.push('/')
            })
            .finally(() => {
                setLoading(false);
            });
    }, [setLoading, history]);

    return <React.Fragment>
        <Loader loading={loading}/>
        <AddLeadForm
            onSubmit={onSubmit}
        />
    </React.Fragment>
}
