import {POST, GET} from "./APIService";
import {toast} from 'react-toastify';


export const saveLead = async (data) => {
    try {
        const response = await POST('/lead', data);

        if (response.error) {
            throw new Error(response.error);
        }

        return response.data;
    } catch (e) {
        toast(e.message, {
            position: toast.POSITION.TOP_LEFT,
            autoClose: 5000,
            type: toast.TYPE.ERROR,
            hideProgressBar: true,
            draggable: false
        });
    }
}

export const getLeads = async (data) => {
    try {
        const response = await GET('/lead', data);

        if (response.error) {
            throw new Error(response.error);
        }

        return response.data;
    } catch (e) {
        toast(e.message, {
            position: toast.POSITION.TOP_RIGHT,
            autoClose: 5000,
            type: toast.TYPE.ERROR,
            hideProgressBar: true,
            draggable: false
        });
    }
}
