import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';

import {
    Switch,
    Route,
    Link,
    BrowserRouter
} from "react-router-dom";

import Home from './components/Home';
import AddLead from './components/AddLead';
import LeadList from './components/LeadList';

const useStyles = makeStyles((theme) => ({
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
        textDecoration: 'none',
        color: 'unset'
    },
    link: {
        margin: theme.spacing(1, 1.5),
        textDecoration: 'none',
        color: 'unset'
    }
}));

export default function Router() {
    const classes = useStyles();

    return (
        <BrowserRouter>
            <CssBaseline/>
            <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                <Toolbar className={classes.toolbar}>
                    <Link to={'/'} className={classes.toolbarTitle}>
                        <Typography variant="h6" color="inherit" noWrap>
                            Leads
                        </Typography>
                    </Link>
                    <nav>
                        <Link variant="button" color="textPrimary" to={'/lead/add'} className={classes.link}>
                            Create Lead
                        </Link>
                        <Link variant="button" color="textPrimary" to={'/lead/list'} className={classes.link}>
                            Show All Leads
                        </Link>
                    </nav>
                </Toolbar>
            </AppBar>
            <div>
                <Switch>
                    <Route path="/lead/list">
                        <LeadList/>
                    </Route>
                    <Route path="/lead/add">
                        <AddLead/>
                    </Route>
                    <Route path="/">
                        <Home/>
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}
